#!/usr/bin/env python3

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="nbUI", # Replace with your own username
    version="0.0.1",
    author="Hugo Chauvet",
    author_email="hugo.chauvet@protonmail.com",
    description="A set of ipywidgets UI for jupyter notebook",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/discosoleil/nbui",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
